import { GET_EXPENSES_SUCCESS, ADD_EXPENSE_SUCCESS } from "./actions";

const initialState = {
  expenses: undefined,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_EXPENSES_SUCCESS:
      return action.payload;
    case ADD_EXPENSE_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export { reducer };
