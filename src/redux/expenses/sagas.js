import { takeEvery, put } from 'redux-saga/effects';
import { GET_EXPENSES, GET_EXPENSES_SUCCESS, ADD_EXPENSE, ADD_EXPENSE_SUCCESS } from './actions';

const base_url = 'http://localhost:5000';

function* handler() {
  yield takeEvery(GET_EXPENSES, getExpenses);
  yield takeEvery(ADD_EXPENSE, addExpense);
}

function* getExpenses(action) {
  try {
    // get expenses
    const expenses = yield fetch(`${base_url}/api/expenses`)
      .then(res => res.json())
      .then(json => json);
    yield put({
      type: GET_EXPENSES_SUCCESS,
      payload: { expenses }
    });
  } catch (e) {
    // handle error
    console.error(e);
  }
}

function* addExpense(action) {
  try {
    // add expense
    const success_msg = "ADD_EXPENSE_SUCCES";
    yield fetch(`${base_url}/api/expense`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: action.payload.data,
    });
    yield put({
      type: ADD_EXPENSE_SUCCESS,
      payload: { success_msg },
    });
  } catch (e) {
    console.error(e);
  }
}

export { handler };
