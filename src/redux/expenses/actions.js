// define action.type(s)
export const GET_EXPENSES = 'GET_EXPENSES';
export const GET_EXPENSES_SUCCESS = 'GET_EXPENSE_SUCCESS';
export const ADD_EXPENSE = 'ADD_EXPENSE';
export const ADD_EXPENSE_SUCCESS = 'ADD_EXPENSE_SUCCESS';
