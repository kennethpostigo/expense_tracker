import { combineReducers } from 'redux';
import { reducer as expenseReducer } from './expenses/reducers';

const reducer = combineReducers({
  expenses: expenseReducer,
});

export { reducer };