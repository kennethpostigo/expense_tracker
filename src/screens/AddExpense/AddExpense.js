import React, { useState } from 'react';
import { ScrollView, Text, View, TextInput } from 'react-native';
import { AddButton } from '../../components';
import CurrencyInput from 'react-native-currency-input';
import DatePicker from 'react-native-date-picker';
import { useDispatch } from 'react-redux';
import { GET_EXPENSES, ADD_EXPENSE } from '../../redux/expenses/actions';
import { connect } from 'react-redux';

import styles from './styles';

const mapStateToProps = (state, props) => {
  const { expenses } = state.expenses;
  return { expenses };
};

const mapDispatchToProps = (dispatch, props) => ({
  getExpenses: () => {
    dispatch({
      type: GET_EXPENSES,
      payload: {},
    });
  },
});

const AddExpenseView = ({ expenses, navigation }) => {
  const { goBack } = navigation;

  const today = new Date()
  const minDate = new Date(2000, 0, 1);

  const [date, setDate] = useState(today);
  const [name, setName] = useState('');
  const [amount, setAmount] = useState(0);
  const [category, setCategory] = useState(null);

  const dispatch = useDispatch();

  return (
    <View style={styles.shell}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.formWrapper}>
          <Text style={styles.formTitle}>Name</Text>
          <TextInput
            numberOfLines={1}
            value={name}
            onChangeText={setName}
            placeholder="Groceries"
            style={styles.nameInput}
          />
          <Text style={styles.formTitle}>Amount</Text>
          <CurrencyInput
            value={amount}
            style={[styles.amountInput, { color: amount === 0 ? '#c4c4c4' : '#000000' }]}
            onChangeValue={val => {
              setAmount(val);
              if (val === null) {
                setAmount(0);
              }
            }}
            prefix="$"
            delimiter=","
            separator="."
            precision={2}
            minValue={0}
          />
          <Text style={styles.formTitle}>Category</Text>
          <TextInput
            numberOfLines={1}
            value={category !== null ? category.toUpperCase() : category}
            onChangeText={setCategory}
            placeholder="Food"
            style={styles.catInput}
          />
          <Text style={styles.formTitle}>Date</Text>
          <DatePicker
            mode="date"
            date={date}
            minimumDate={minDate}
            onDateChange={setDate}
            style={styles.datePicker}
          />
        </View>
      </ScrollView>
      <AddButton
        positionStyle={styles.addButton}
        text="Add Expense"
        onPress={() => {
          if (amount === 0) {
            // add validation message to screen
            // instead of goBack() to expenses
            console.log('amount is 0, must display form error');
            goBack();
          }
          const dateJson = date.toISOString();
          const amtJson = amount.toString();
          console.log(dateJson);
          const data = JSON.stringify({
            id: expenses.length + 1,
            name: name,
            amount: amtJson,
            category: category.toUpperCase(),
            date: dateJson,
          });
          console.log(data);
          dispatch({
            "type": ADD_EXPENSE,
            "payload": { data },
          });
          goBack();
        }}
      />
    </View>
  );
};

const AddExpense = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddExpenseView);

export default AddExpense;
