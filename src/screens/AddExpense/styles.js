import { StyleSheet } from "react-native";

export default StyleSheet.create({
  shell: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  formWrapper: {
    margin: 20,
  },
  formTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 0.8,
    color: '#117979',
    marginTop: 8,
  },
  datePicker: {
    alignSelf: 'center',
  },
  nameInput: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    margin: 15,
    borderWidth: 0.5,
    borderColor: '#1ebaba',
    paddingHorizontal: 12,
    borderRadius: 4,
    backgroundColor: 'white',
  },
  amountInput: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    margin: 15,
    borderWidth: 0.5,
    borderColor: '#1ebaba',
    paddingHorizontal: 12,
    borderRadius: 4,
    backgroundColor: 'white',
  },
  catInput: {
    alignSelf: 'center',
    width: '80%',
    height: 40,
    margin: 15,
    borderWidth: 0.5,
    borderColor: '#1ebaba',
    paddingHorizontal: 12,
    borderRadius: 4,
    backgroundColor: 'white',
  },
  addButton: {
    position: 'absolute',
    bottom: 45,
    alignSelf: 'center',
  }
});
