import React, { useEffect, useState } from 'react';
import {
  ScrollView, Text, View, TouchableOpacity, Alert,
} from 'react-native';
import CurrencyInput from 'react-native-currency-input';
import { AddButton } from '../../components';
import { connect } from 'react-redux';

import styles from './styles';
import { GET_EXPENSES } from '../../redux/expenses/actions';

// get unique category values
const getCategories = exp => [...new Set(exp.map(i => i.category))].sort();

// split array into specified length segments
const splitArray = (arr, len) => {
  var newArr = [];
  let i = 0;
  let n = arr.length;
  while (i < n) {
    newArr.push(arr.slice(i, i += len));
  }
  return newArr;
};

const isInt = (val) => /^\d+$/.test(val);

const mapStateToProps = (state, props) => {
  const { expenses } = state.expenses;
  return { expenses };
};

const mapDispatchToProps = (dispatch, props) => ({
  getExpenses: () => {
    dispatch({
      type: GET_EXPENSES,
      payload: 'GET_EXPENSES_SUCCESS',
    });
  },
});

const createAlert = results => {
  let sum = 0;
  let text = '';
  results.forEach(i => {
    sum += parseFloat(i.amount);
    text += `Transaction ${i.id}\n`;
    text += `Amount ${i.amount}\n`;
    text += `Name ${i.name}\n`;
    text += `Category ${i.category}\n`;
    text += `Date ${i.date}\n\n`;
  });
  return Alert.alert(
    `Spent ${sum}`,
    text,
    [
      {
        text: "OK",
        onPress: () => { },
      }
    ]
  );
};

const ExpenseView = ({ expenses, getExpenses, navigation }) => {
  const today = new Date();

  // get updated expense list when main screen goes into focus
  useEffect(() => {
    var unsubscribe;
    expenses !== undefined
      ? setTimeout(() => {
        unsubscribe = navigation.addListener('focus', () => {
          getExpenses();
        });
      }, 500)
      : unsubscribe = navigation.addListener('focus', () => {
        getExpenses();
      });
    return unsubscribe;
  }, [navigation]);

  // result to display
  var results = expenses;

  const categories = expenses === undefined ? null : getCategories(expenses);
  // split categories into subarrays for rendering views
  var catSplit = categories === null ? null : splitArray(categories, 4);

  // sort array by date or not based on sortDate state
  const [sortDate, setSortDate] = useState(false);
  if (sortDate) {
    expenses.sort((a, b) => {
      let aD = new Date(a.date);
      let bD = new Date(b.date);
      return bD - aD;
    });
  } else {
    expenses ? expenses.sort((a, b) => a.id - b.id) : expenses;
  }

  // filter by time
  const [time, setTime] = useState('all');
  const [minTime, setMinTime] = useState(null);
  const [maxTime, setMaxTime] = useState(null);
  const [minMonth, setMinMonth] = useState(0); // 0-11 for Date()
  const [minDay, setMinDay] = useState(0); // 1-31
  const [minYear, setMinYear] = useState(0);
  const [maxMonth, setMaxMonth] = useState(0); // 0-11
  const [maxDay, setMaxDay] = useState(0); // 1-31
  const [maxYear, setMaxYear] = useState(0);

  // filter by category
  const [category, setCategory] = useState('all');
  const [selectedCat, setSelectedCat] = useState(null);

  results = category !== 'all'
    ? results.filter(i => i.category === category)
    : results;

  switch (time) {
    case 'week':
      results = results.filter(i => {
        let date = new Date(i.date);
        let year = today.getFullYear();
        let month = today.getMonth();
        let day = today.getDate();
        let minRange = new Date(year, month, day - today.getDay());
        return date >= minRange;
      });
    case 'month':
      results = results.filter(i => {
        let date = new Date(i.date);
        let year = today.getFullYear();
        let month = today.getMonth();
        let minRange = new Date(year, month, 1);
        return date >= minRange;
      });
    default:
    // do something with min/maxTime 
  }

  // hacky way of getting sum...
  // reduce() was getting undefined values for random entries
  var sum = 0;
  results && results.forEach(i => { sum += parseFloat(i.amount) });

  return (
    <View style={styles.shell}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View style={styles.div}>
          {categories && categories.length >= 1
            && (
              <View style={styles.catSortTable}>
                {catSplit.map((catGroup, rowId) => (
                  <View style={styles.catRow} key={rowId}>
                    {catGroup.map((cat, catId) => (
                      <TouchableOpacity
                        onPress={() => {
                          if (selectedCat === `${rowId},${catId}`) {
                            setSelectedCat(null)
                            setCategory('all');
                          } else {
                            setSelectedCat(`${rowId},${catId}`)
                            setCategory(cat);
                          }
                        }}
                        key={catId}
                      >
                        <View
                          style={
                            selectedCat === `${rowId},${catId}`
                              ? styles.selectedPill
                              : styles.pill
                          }
                        >
                          <Text
                            numberOfLines={1}
                            style={styles.pillText}
                          >
                            {cat}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    ))}
                  </View>
                ))}
              </View>
            )}
          {results && results.length >= 1
            && (
              <View style={styles.expensesView}>
                <View style={styles.sortRow}>
                  <Text style={styles.sortLabel}>
                    Sort By:
                  </Text>
                  <View style={styles.sortBtns}>
                    <TouchableOpacity
                      onPress={() => setSortDate(!sortDate)}
                    >
                      <View
                        style={styles.sortButton}
                      >
                        <Text
                          numberOfLines={1}
                          style={styles.sortButtonText}
                        >
                          {sortDate ? 'Entry' : 'Date'}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        time === 'month' ? setTime('all') : setTime('month');
                      }}
                    >
                      <View
                        style={styles.sortButton}
                      >
                        <Text
                          numberOfLines={1}
                          style={styles.sortButtonText}
                        >
                          Past Month
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        time === 'week' ? setTime('all') : setTime('week');
                      }}
                    >
                      <View
                        style={styles.sortButton}
                      >
                        <Text
                          numberOfLines={1}
                          style={styles.sortButtonText}
                        >
                          Past Week
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.timeRangeView}>
                  <View style={styles.range}>
                    <View style={styles.col}>
                      <Text style={{ marginBottom: 10 }}>Minimum Range</Text>
                      <View style={styles.minRange}>
                        <CurrencyInput
                          value={minMonth}
                          style={
                            [styles.dateInput,
                            { color: minMonth === 0 ? '#c4c4c4' : 'black' }]
                          }
                          onChangeValue={val => {
                            setMinMonth(val);
                            if (val === null) {
                              setMinMonth(0);
                            }
                          }}
                          prefix=""
                          minValue={1}
                          maxValue={12}
                          precision={0}
                        />
                        <CurrencyInput
                          value={minDay}
                          style={
                            [styles.dateInput,
                            { color: minDay === 0 ? '#c4c4c4' : 'black' }]
                          }
                          onChangeValue={val => {
                            setMinDay(val);
                            if (val === null) {
                              setMinDay(0);
                            }
                          }}
                          prefix=""
                          minValue={1}
                          maxValue={31}
                          precision={0}
                        />
                        <CurrencyInput
                          value={minYear}
                          style={
                            [styles.yearInput,
                            { color: minYear === 0 ? '#c4c4c4' : 'black' }]
                          }
                          onChangeValue={val => {
                            setMinYear(val);
                            if (val === null) {
                              setMinYear(0);
                            }
                          }}
                          prefix=""
                          delimiter=""
                          maxValue={today.getFullYear()}
                          precision={0}
                        />
                      </View>
                    </View>
                    <View style={styles.col}>
                      <Text style={{ marginBottom: 10 }}>Maximium Range</Text>
                      <View style={styles.maxRange}>
                        <CurrencyInput
                          value={maxMonth}
                          style={
                            [styles.dateInput,
                            { color: maxMonth === 0 ? '#c4c4c4' : 'black' }]
                          }
                          onChangeValue={val => {
                            setMaxMonth(val);
                            if (val === null) {
                              setMaxMonth(0);
                            }
                          }}
                          prefix=""
                          minValue={1}
                          maxValue={12}
                          precision={0}
                        />
                        <CurrencyInput
                          value={maxDay}
                          style={
                            [styles.dateInput,
                            { color: maxDay === 0 ? '#c4c4c4' : 'black' }]
                          }
                          onChangeValue={val => {
                            setMaxDay(val);
                            if (val === null) {
                              setMaxDay(0);
                            }
                          }}
                          prefix=""
                          minValue={1}
                          maxValue={31}
                          precision={0}
                        />
                        <CurrencyInput
                          value={maxYear}
                          style={
                            [styles.yearInput,
                            { color: maxYear === 0 ? '#c4c4c4' : 'black' }]
                          }
                          onChangeValue={val => {
                            setMaxYear(val);
                            if (val === null) {
                              setMaxYear(0);
                            }
                          }}
                          prefix=""
                          delimiter=""
                          maxValue={today.getFullYear()}
                          precision={0}
                        />
                      </View>
                    </View>
                  </View>
                  <View style={styles.goRange}>
                    <TouchableOpacity
                      onPress={() => {
                        let mD = minDay === 0 ? 1 : minDay;
                        let mM = minMonth === 12 ? 11 : minMonth - 1;
                        let mY = minYear === 0 ? 2000 : minYear;
                        let mDate = new Date(mY, mM, mD);
                        let MD = maxDay === 0 ? 1 : maxDay;
                        let MM = maxMonth === 12 ? 11 : maxMonth - 1;
                        let MY = maxYear === 0 ? today.getFullYear() : maxYear;
                        let MDate = new Date(MY, MM, MD);
                        results = results.filter(i => {
                          let date = new Date(i.date);
                          return date >= mDate && date <= MDate;
                        });
                        createAlert(results);
                      }}
                    >
                      <View
                        style={styles.sortButton}
                      >
                        <Text
                          numberOfLines={1}
                          style={styles.sortButtonText}
                        >
                          Range
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.spentView}>
                  <Text style={styles.spentLabel}>
                    Spent: ${sum.toFixed(2)}
                  </Text>
                </View>
                <View style={styles.expenseTable}>
                  {results.map(expense => {
                    const { id, name, amount, category, date } = expense;
                    const nDate = new Date(date);
                    const displayDate = `${nDate.getMonth() + 1}/${nDate.getDate()}/${nDate.getFullYear()}`;
                    return (
                      <View style={styles.expenseCard} key={id}>
                        <View style={styles.leftOfCard}>
                          <View style={styles.idWrapper}>
                            <Text style={styles.idText}>{id}</Text>
                          </View>
                          <View style={styles.infoWrapper}>
                            <Text style={styles.catText}>{category}</Text>
                            <Text style={styles.nameText}>{name}</Text>
                            <Text style={styles.dateText}>{displayDate}</Text>
                          </View>
                        </View>
                        <View style={styles.rightOfCard}>
                          <Text style={styles.amountText}>${isInt(amount) ? parseInt(amount).toFixed(2) : parseFloat(amount).toFixed(2)}</Text>
                        </View>
                      </View>
                    );
                  })}
                </View>
              </View>
            )}
          {results && results.length === 0 && (
            <View style={styles.emptyView}>
              <Text>
                No expenses yet, add a new expense with the "Add Expense" button.
              </Text>
            </View>
          )}
        </View>
      </ScrollView>
      <AddButton
        positionStyle={styles.addButton}
        onPress={() => navigation.navigate('Add Expense', { expenses: expenses })}
        text="Add Expense"
      />
    </View>
  );
};

const ExpensesView = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExpenseView);

export default ExpensesView;
