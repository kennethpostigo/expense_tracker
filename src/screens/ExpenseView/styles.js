import { StyleSheet } from "react-native";

export default StyleSheet.create({
  shell: {
    flex: 1,
  },
  scrollView: {
    paddingBottom: 50,
  },
  div: {
    flex: 1,
  },
  sortButton: {
    height: 45,
    marginBottom: 10,
    justifyContent: 'center',
    backgroundColor: '#1ebaba',
    width: '20%',
    alignSelf: 'center',
    borderRadius: 4,
  },
  catSortTable: {
    flex: 1,
    alignItems: 'center',
    marginBottom: 20,
  },
  catRow: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    marginTop: 10,
  },
  pill: {
    height: 35,
    maxWidth: 110,
    borderRadius: 20,
    paddingHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#117979',
  },
  selectedPill: {
    height: 35,
    maxWidth: 110,
    borderRadius: 20,
    paddingHorizontal: 20,
    justifyContent: 'center',
    backgroundColor: '#1ebaba',
  },
  pillText: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
  },
  sortRow: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 15,
  },
  sortLabel: {
    alignSelf: 'center',
    fontSize: 20,
    marginLeft: 20,
  },
  sortBtns: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-evenly',
  },
  sortButton: {
    height: 40,
    backgroundColor: '#1ebaba',
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderRadius: 4,
  },
  sortButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  timeRangeView: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  range: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  col: {
    flex: 1,
    alignItems: 'center',
  },
  minRange: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  maxRange: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  goRange: {
    marginRight: 5,
    alignSelf: 'center',
  },
  dateInput: {
    width: 45,
    height: 35,
    borderWidth: 0.5,
    borderColor: '#1ebaba',
    paddingHorizontal: 12,
    borderRadius: 4,
    backgroundColor: 'white',
  },
  yearInput: {
    width: 60,
    height: 35,
    borderWidth: 0.5,
    borderColor: '#1ebaba',
    paddingHorizontal: 12,
    borderRadius: 4,
    backgroundColor: 'white',
  },
  spentLabel: {
    fontSize: 24,
    fontWeight: 'bold',
    marginVertical: 15,
    textAlign: 'center',
  },
  expenseTable: {
    flex: 1,
  },
  expenseCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginVertical: 5,
  },
  leftOfCard: {
    flexDirection: 'row',
  },
  idWrapper: {
    alignSelf: 'center',
    marginRight: 20,
    marginLeft: 5,
  },
  idText: {
    color: '#869297',
  },
  catText: {
    textTransform: 'uppercase',
    fontSize: 12,
    fontWeight: '500',
  },
  nameText: {
    fontSize: 20,
    fontWeight: '600',
    color: '#117979',
  },
  dateText: {
    color: '#869297',
  },
  rightOfCard: {
    alignSelf: 'center',
  },
  amountText: {
    fontSize: 18,
    marginRight: 18,
  },
  addButtonContainer: {
    marginTop: 10,
  },
  addButton: {
    bottom: 35,
    alignSelf: 'center',
  },
  touchO: {
    height: 200,
    backgroundColor: 'black',
  },
});
