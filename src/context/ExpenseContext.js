import createDataContext from "./createDataContext";
import { AsyncStorage } from '@react-native-community/async-storage';

const expenseReducer = (state, action) => {
  switch (action.type) {
    case 'get_expenses':
      return action.payload;
    case 'update_expenses':
      return action.payload;
    default:
      return state;
  }
};

const getExpenses = (dispatch) => {
  return async () => {
    let expenses;
    try {
      const stored_expenses = await AsyncStorage.getItem('expenses');
      expenses = JSON.parse(stored_expenses);
    } catch (e) {
      console.log(
        'getExpenses [Error]: Could not get expenses from AsyncStorage',
        `getExpenses [Error]: ${e}`,
        'getExpenses [Error]: Clearing out AsyncStorage',
      );
      expenses = [];
      await AsyncStorage.setItem('expenses', JSON.stringify(expenses));
    }
    dispatch({ type: 'get_expenses', payload: { expenses } });
  };
};

const updateExpenses = (dispatch) => {
  return async (new_expenses) => {
    await AsyncStorage.setItem('expenses', JSON.stringify(new_expenses));
    dispatch({ type: 'update_expenses', payload: { expenses: new_expenses } });
  };
};

export const { Context, Provider } = createDataContext(
  expenseReducer,
  { getExpenses, updateExpenses },
  { expenses: []},
);
