import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

import styles from './styles';

const AddButton = (props) => {
  const {
    onPress,
    positionStyle,
    buttonStyle,
    text,
  } = props;

  return (
    <TouchableOpacity onPress={onPress} style={positionStyle}>
      <View style={[styles.button, buttonStyle]}>
        <Text
          numberOfLines={1}
          style={styles.buttonText}
        >
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default AddButton;
