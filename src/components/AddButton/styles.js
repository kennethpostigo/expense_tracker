import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  button: {
    height: 50,
    minWidth: 64,
    paddingHorizontal: 35,
    borderRadius: 4,
    justifyContent: 'center',
    backgroundColor: '#1ebaba',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
