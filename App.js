/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ExpenseView, AddExpense } from './src/screens';

import { store } from './src/redux/store';
import { Provider } from 'react-redux';

const Stack = createNativeStackNavigator();

const ExpenseStack = () => {
  return (
    <Stack.Navigator initialRouteName="Expenses">
      <Stack.Screen
        name="Expenses"
        options={{
          headerTintColor: 'white',
          headerTitleStyle: {
            fontWeight: '800',
          },
          headerStyle: {
            backgroundColor: '#117979',
          },
        }}
        component={ExpenseView}
      />
      <Stack.Screen
        name="Add Expense"
        options={{
          headerTintColor: '#117979',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}
        component={AddExpense}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <ExpenseStack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
