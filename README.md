## expense_tracker
react native app to track expenses using Redux and Redux Sagas

![main screen](https://bitbucket.org/kennethpostigo/expense_tracker/raw/6de416b781d0ac9e531bce0f03fdefd5d9ab265f/assets/screenshots/main_screen.png)

![add screen](https://bitbucket.org/kennethpostigo/expense_tracker/raw/6de416b781d0ac9e531bce0f03fdefd5d9ab265f/assets/screenshots/add_screen.png)

## Installation
In the root directory, run `yarn install`, then `cd ios` and run `pod install`.

### iOS:
• In root directory, run `yarn ios` to run the app in a simulator. Make sure [expense_server](https://bitbucket.org/kennethpostigo/expense_server/src/master/) is running.

### Android:
This app was not simulated or tested on android YET, so use at your own discretion.


• Have an Android emulator running (quickest way to get started), or a device connected.
• cd "/path/to/expense_tracker" && npx react-native run-android

## Assumptions
I initially decided to use React Hooks (useContext) and AsyncStorage to solve this challenge, but I was unable to resolve a common issue where the useContext hook was returning undefined due to something that had to do with the initial state.  With that frustration and understanding that Hooks are not a replacement to state management, I decided to implement the recommended libraries: Redux and Redux Sagas.

An Expense object in this project is considered an object with an `id` , `name`, `amount`, `date`, and `category`.

I attempted to build the app to allow the user to choose a category from a dropdown, with the option to add additional categories through a TextInput. I left that functionality to the side so that I could focus on building the actual task. 

For the sake of simplicity I used TextInputs for all fields except for the date, since I would need that as a Date object to be able to filter expense results later on. I included input validation for the `amount` field to only allow numbers and to display as USD.

For the main screen, I display the entry value (id), category, name, date, and amount for each transaction. The transaction history is taken from the server I built mentioned above. I added a "Sort By Entry/Date" button in order to display the ability to filter Dates on the front end, and I have included a `findByCat` method in my server to demonstrate my ability to filter expenses by category. This how I demonstrated the ability to incorporate a way to answer the question "how much was spent on $category in the past month/week/<time window>".

## Revised Update
Included some validations for date and currency values, may still have more to comb through.  I added an alert for showing transactions within a specified date range just for proof of concept, in the future I would have those transactions show up on the main expenses screen with a rerender.
